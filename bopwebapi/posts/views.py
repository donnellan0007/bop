from django.shortcuts import render,get_object_or_404,redirect
from .models import Post,Comment
from posts import models
from .forms import PostForm,CommentForm
from django.views.generic import ListView,DetailView,UpdateView,DeleteView,RedirectView,CreateView
# Create your views here.


def home(request):
    return render(request,'posts/index.html')

class PostListView(ListView):
    template_name = 'posts/post_list.html'
    model = Post
    def get(self,request):
        posts = Post.objects.all()
        context = {'posts':posts}
        return render(request,self.template_name,context)

    def get_queryset(self):
        return Post.objects.all()

class PostDetailView(DetailView):
    model = Post
    template_name = 'posts/post_detail.html'


class PostUpdateView(UpdateView):
    model = Post
    fields = ['description','thumbnail']
    redirect_field_name = 'posts/post_list.html/'
    template_name = 'posts/post_update.html'

    def test_func(self):
        if request.user == post.author:
            return True
        else:
            return False



class PostFormView(CreateView):
    model = Post
    template_name = 'posts/post_form.html'
    redirect_field_name = 'posts/post_list.html/'
    form_class = PostForm

    def form_valid(self,form):
        form.instance.author = self.request.user
        return super().form_valid(form)

def add_comment_to_post(request,pk):
    post = get_object_or_404(Post,pk=pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.author = request.user
            comment.save()
            return redirect('posts:post-detail',pk=post.pk)
    else:
        form = CommentForm
    return render(request,'posts/comment_form.html',{'form':form})   

def get_queryset(self):
    # return Comment.objects.filter(created_date__lte=timezone.now()).order_by('-created_date')
    return Comment.objects.all()



def graph_view(request):
    import matplotlib.pyplot as plt

    page_views = [23,495,958,948,2012,942]
    post_likes = [8,128,491,821,1892,213]

    lines = plt.plot(page_views,post_likes)
    plt.grid(True)
    plt.setp(lines,color=(1,.4,.4),marker='o',mec=(62/255, 15/255, 138/255),mew=(1.25),picker=(0.4))

    plt.xlabel('Page Views')
    plt.ylabel('Post Likes')
    plotgraph1 = plt.savefig(f'media/post_images/filename{request.user}.png')
    plt.show()

    return render(request,'posts/graph.html',{'plt':plotgraph1})


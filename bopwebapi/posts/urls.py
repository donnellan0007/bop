"""bopwebapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from .views import home,PostListView,PostFormView,PostDetailView,add_comment_to_post,graph_view,PostUpdateView
from posts import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

app_name = 'posts'

urlpatterns = [
    path('',views.PostListView.as_view(),name='posts'),
    path('all/',views.PostListView.as_view(),name='posts'),
    path('new/',views.PostFormView.as_view(),name='create-post'),
    path('<int:pk>/',views.PostDetailView.as_view(),name='post-detail'),
    path('<int:pk>/comment',views.add_comment_to_post,name='add-comment'),
    path('<int:pk>/update/',views.PostUpdateView.as_view(),name='update-post'),
    path('stats/',views.graph_view,name='graph'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

from django import forms
from .models import Post,Comment
from .views import *


class PostForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super(PostForm,self).__init__(*args,**kwargs)
        self.fields['video'].required = True
        self.fields['thumbnail'].required = True
        self.fields['description'].required = True
    
    class Meta:
        model = Post
        fields = ['video','thumbnail','description']

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']

    def __init__(self,*args,**kwargs):
        super(CommentForm,self).__init__(*args,**kwargs)
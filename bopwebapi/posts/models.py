from django.db import models
from PIL import Image
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.urls import reverse
from django.utils import timesince,timezone
from django.contrib.auth.models import User
from .validators import validate_file_extension
# Create your models here.

class Post(models.Model):
    author = models.ForeignKey(User,on_delete=models.CASCADE)  # change eventually to logged in user
    description = models.TextField(max_length=60)
    image = ProcessedImageField(upload_to='post_images',
                                        processors=[ResizeToFill(350,350)],
                                        format='JPEG',
                                        default='default.jpg',
                                        options={'quality':60},
                                        )
    thumbnail = ProcessedImageField(upload_to='post_thumbnails',
                                        processors=[ResizeToFill(1280,720)],
                                        format='JPEG',
                                        options={'quality':60},
                                        null = True,
                                        blank = True,
                                        )
    views = models.IntegerField(default=0)                                    
    video = models.FileField(default='default.mp4',upload_to="post_images", validators=[validate_file_extension])                                    
    published_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return self.description
    
    def save(self,*args,**kwargs):
        super().save(*args,**kwargs)
    
    def  get_absolute_url(self):
        return reverse('posts:posts')

class Comment(models.Model):
    author = models.ForeignKey(User,on_delete=models.CASCADE)
    text = models.TextField(max_length=300)
    post = models.ForeignKey('posts.Post',on_delete=models.CASCADE,related_name='comments')
    

    def __str__(self):
        return self.text

    def save(self,*args,**kwargs):
        super().save(*args,**kwargs)
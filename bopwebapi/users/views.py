from django.shortcuts import render,get_object_or_404,redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse,reverse_lazy
from django.contrib.auth.models import User
from .models import Profile
from posts.models import Post
from django.db.models import Q
from django.views.generic import ListView,DetailView,UpdateView,DeleteView,RedirectView,CreateView
from django.contrib.auth import authenticate,login,logout
from .forms import UserProfileInfoForm,UserUpdateForm,ProfileUpdateForm
# Create your views here.

@login_required(login_url='users/user-login')
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('users:user-login'))

def register(request):
    if request.method == 'POST':
        form = UserProfileInfoForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            return redirect('users:user-login')
    else:
        form = UserProfileInfoForm()
    return render(request,'users/registration.html',{'form':form})

class PostLikeRedirect(RedirectView):
    def get_redirect_url(self,*args,**kwargs):
        pk = self.kwargs.get("pk")
        print(pk) #dev purposes
        obj = get_object_or_404(User,pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            if user in obj.subscribers.all():
                obj.subscribers.remove(user)
            else:
                obj.subscribers.add(user)
        return url_


def profile_update(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST,instance=request.user)
        p_form = ProfileUpdateForm(request.POST,request.FILES,instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            return redirect('posts:posts')
    else:
        u_form = UserUpdateForm(request.POST,instance=request.user)
        p_form = ProfileUpdateForm(request.POST,request.FILES,instance=request.user.profile)
    
    context = {
        'u_form':u_form,
        'p_form':p_form,
    }
    return render(request,'users/profile_update.html',context)


def view_profile(request,pk=None):
        if pk:
            user_profile = User.objects.get(pk=pk)
            user_posts = Post.objects.filter(author__id=pk).order_by('-published_date')
        else:
            user_profile = request.user
            user_posts = Post.objects.filter(author__id = request.user.id).order_by('-published_date')
        context = {
                   'user':user_profile,
                   'user_posts':user_posts,
                  }
        return render(request,'users/profile.html',context)


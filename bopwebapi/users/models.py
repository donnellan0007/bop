from django.db import models
from django.urls import reverse
from PIL import Image
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
import uuid
from django.utils import timesince,timezone

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,max_length=100)
    description = models.TextField(max_length=100)
    subscribers = models.ManyToManyField(User,related_name='subscribers')
    image = ProcessedImageField(upload_to='profile_pics',
                                            processors=[ResizeToFill(150,150)],
                                            default='default_profile.jpg',
                                            format='JPEG',
                                            options={'quality':60})

    def __str__(self):
        return f'{self.user.username} Profile'
    
    def save(self,*args,**kwargs):
        super().save(*args,**kwargs)

@receiver(post_save,sender=User)
def create_or_update_user_profile(sender,instance,created,**kwargs):
    if created:
        Profile.objects.create(user=instance)
    else:
        instance.profile.save()
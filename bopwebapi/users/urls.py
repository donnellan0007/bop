"""bopwebapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from users import views
from .views import user_logout,register,profile_update,view_profile,PostLikeRedirect
from .forms import UserProfileInfoForm
from users import forms
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

app_name = 'users'

urlpatterns = [
    path('login/',auth_views.LoginView.as_view(template_name='users/login.html'),name='user-login'),
    path('logout/',views.user_logout,name='logout'),
    path('register/',views.register,name='register'),
    path('profile/update/',views.profile_update,name='profile-update'),
    path('account/',views.view_profile,name='view_profile'),
    path('account/<pk>/',views.view_profile,name='view_profile_with_pk'),
    path('account/<pk>/subscribe',views.PostLikeRedirect.as_view(),name='subscribe'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
